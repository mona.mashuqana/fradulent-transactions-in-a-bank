# Fradulent Transactions in a Bank



American multinational banking and financial services company,With over 2,37,200 employees, based in 50 countries, they handle over 32 million clients throughout the world on a daily basis.
They provide services like retail banking, corporate and investment banking, asset management, portfolio management, insurance and other financial services.
While dealing with innumerable money transactions, they’ve set up an internal team which closely monitors and alarms the transactions which could be deemed fraudulent.
